<?php

/**
 * Plugin Name: Base Stardente
 * Description: Controle base do tema Stardente.
 * Version: 0.1
 * Author: Agência HC Desenvolvimentos
 * Author URI: http://hcdesenvolvimentos.com.br
 * Licence: GPL2
 */


	function baseStardente () {

		// TIPOS DE CONTEÚDO
		conteudosStardente();

		taxonomiaStardente();

		metaboxesStardente();
	}

	/****************************************************
	* TIPOS DE CONTEÚDO
	*****************************************************/

	function conteudosStardente (){

		// TIPOS DE CONTEÚDO
		tipoDestaque();
		// tipoTratamentosClinicos();
		tipoTratamento();
		tipoDepoimentos();
		tipoLocalizacao();
		tipoTime();

		/* ALTERAÇÃO DO TÍTULO PADRÃO */
		add_filter( 'enter_title_here', 'trocarTituloPadrao' );
		function trocarTituloPadrao($titulo){

			switch (get_current_screen()->post_type) {

				case 'destaque':
					$titulo = 'Título do destaque';
				break;
				
				default:
				break;
			}

		    return $titulo;

		}

	}

	// CUSTOM POST TYPE DESTAQUES
	function tipoDestaque() {

		$rotulosDestaque = array(
								'name'               => 'Destaques',
								'singular_name'      => 'destaque',
								'menu_name'          => 'Destaques',
								'name_admin_bar'     => 'Destaques',
								'add_new'            => 'Adicionar novo',
								'add_new_item'       => 'Adicionar novo destaque',
								'new_item'           => 'Novo destaque',
								'edit_item'          => 'Editar destaque',
								'view_item'          => 'Ver destaque',
								'all_items'          => 'Todos os destaques',
								'search_items'       => 'Buscar destaque',
								'parent_item_colon'  => 'Dos destaques',
								'not_found'          => 'Nenhum destaque cadastrado.',
								'not_found_in_trash' => 'Nenhum destaque na lixeira.'
							);

		$argsDestaque 	= array(
								'labels'             => $rotulosDestaque,
								'public'             => true,
								'publicly_queryable' => true,
								'show_ui'            => true,
								'show_in_menu'       => true,
								'menu_position'		 => 4,
								'menu_icon'          => 'dashicons-star-filled',
								'query_var'          => true,
								'rewrite'            => array( 'slug' => 'destaque' ),
								'capability_type'    => 'post',
								'has_archive'        => true,
								'hierarchical'       => false,
								'supports'           => array( 'title','thumbnail','editor' )
							);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('destaque', $argsDestaque);

	}

	// CUSTOM POST TYPE TRATAMENTOS CLINICOS
	// function tipoTratamentosClinicos() {

	// 	$rotulosTratamentosClinicos = array(
	// 							'name'               => 'Tratamentos clinicos',
	// 							'singular_name'      => 'tratamento clinico',
	// 							'menu_name'          => 'Tratamentos clinicos',
	// 							'name_admin_bar'     => 'Tratamentos clinicos',
	// 							'add_new'            => 'Adicionar novo',
	// 							'add_new_item'       => 'Adicionar novo tratamento clinico',
	// 							'new_item'           => 'Novo tratamento clinico',
	// 							'edit_item'          => 'Editar tratamento clinico',
	// 							'view_item'          => 'Ver tratamento clinico',
	// 							'all_items'          => 'Todos os tratamento clinicos',
	// 							'search_items'       => 'Buscar tratamento clinico',
	// 							'parent_item_colon'  => 'Dos tratamento clinicos',
	// 							'not_found'          => 'Nenhum tratamento clinico cadastrado.',
	// 							'not_found_in_trash' => 'Nenhum tratamento clinico na lixeira.'
	// 						);

	// 	$argsTratamentosClinicos 	= array(
	// 							'labels'             => $rotulosTratamentosClinicos,
	// 							'public'             => true,
	// 							'publicly_queryable' => true,
	// 							'show_ui'            => true,
	// 							'show_in_menu'       => true,
	// 							'menu_position'		 => 5,
	// 							'menu_icon'          => 'dashicons-admin-generic',
	// 							'query_var'          => true,
	// 							'rewrite'            => array( 'slug' => 'tratamento-clinico' ),
	// 							'capability_type'    => 'post',
	// 							'has_archive'        => true,
	// 							'hierarchical'       => false,
	// 							'supports'           => array( 'title','thumbnail','editor' )
	// 						);

	// 	// REGISTRA O TIPO CUSTOMIZADO
	// 	register_post_type('tratamento-clinico', $argsTratamentosClinicos);

	// }

	// CUSTOM POST TYPE TRATAMENTOS ESTÉTICOS
	function tipoTratamento() {

		$rotulosTratamentosEsteticos = array(
								'name'               => 'Tratamentos',
								'singular_name'      => 'tratamento',
								'menu_name'          => 'Tratamentos',
								'name_admin_bar'     => 'Tratamentos',
								'add_new'            => 'Adicionar novo',
								'add_new_item'       => 'Adicionar novo tratamento',
								'new_item'           => 'Novo tratamento',
								'edit_item'          => 'Editar tratamento',
								'view_item'          => 'Ver tratamento',
								'all_items'          => 'Todos os tratamento',
								'search_items'       => 'Buscar tratamento',
								'parent_item_colon'  => 'Dos tratamento',
								'not_found'          => 'Nenhum tratamento cadastrado.',
								'not_found_in_trash' => 'Nenhum tratamento na lixeira.'
							);

		$argsTratamentosEsteticos 	= array(
								'labels'             => $rotulosTratamentosEsteticos,
								'public'             => true,
								'publicly_queryable' => true,
								'show_ui'            => true,
								'show_in_menu'       => true,
								'menu_position'		 => 6,
								'menu_icon'          => 'dashicons-smiley',
								'query_var'          => true,
								'rewrite'            => array( 'slug' => 'tratamentos' ),
								'capability_type'    => 'post',
								'has_archive'        => true,
								'hierarchical'       => false,
								'supports'           => array( 'title','thumbnail','editor' )
							);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('tratamento', $argsTratamentosEsteticos);

	}

	// CUSTOM POST TYPE TRATAMENTOS CLINICOS
	function tipoDepoimentos() {

		$rotulosDepoimentos = array(
								'name'               => 'Depoimentos',
								'singular_name'      => 'depoimento',
								'menu_name'          => 'Depoimentos',
								'name_admin_bar'     => 'Depoimentos',
								'add_new'            => 'Adicionar novo',
								'add_new_item'       => 'Adicionar novo depoimento',
								'new_item'           => 'Novo depoimento',
								'edit_item'          => 'Editar depoimento',
								'view_item'          => 'Ver depoimento',
								'all_items'          => 'Todos os depoimentos',
								'search_items'       => 'Buscar depoimento',
								'parent_item_colon'  => 'Dos depoimentos',
								'not_found'          => 'Nenhum depoimento cadastrado.',
								'not_found_in_trash' => 'Nenhum depoimento na lixeira.'
							);

		$argsDepoimentos 	= array(
								'labels'             => $rotulosDepoimentos,
								'public'             => true,
								'publicly_queryable' => true,
								'show_ui'            => true,
								'show_in_menu'       => true,
								'menu_position'		 => 7,
								'menu_icon'          => 'dashicons-format-quote',
								'query_var'          => true,
								'rewrite'            => array( 'slug' => 'depoimento' ),
								'capability_type'    => 'post',
								'has_archive'        => true,
								'hierarchical'       => false,
								'supports'           => array( 'title','thumbnail','editor' )
							);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('depoimento', $argsDepoimentos);

	}
	
	// CUSTOM POST TYPE LOCALIZAÇÃO
	function tipoLocalizacao() {

		$rotulosLocalizacao = array(
								'name'               => 'Localização',
								'singular_name'      => 'localização',
								'menu_name'          => 'Localização',
								'name_admin_bar'     => 'Localização',
								'add_new'            => 'Adicionar novo',
								'add_new_item'       => 'Adicionar novo localização',
								'new_item'           => 'Novo localização',
								'edit_item'          => 'Editar localização',
								'view_item'          => 'Ver localização',
								'all_items'          => 'Todos os localizaçãos',
								'search_items'       => 'Buscar localização',
								'parent_item_colon'  => 'Dos localizaçãos',
								'not_found'          => 'Nenhum localização cadastrado.',
								'not_found_in_trash' => 'Nenhum localização na lixeira.'
							);

		$argsLocalizacao 	= array(
								'labels'             => $rotulosLocalizacao,
								'public'             => true,
								'publicly_queryable' => true,
								'show_ui'            => true,
								'show_in_menu'       => true,
								'menu_position'		 => 8,
								'menu_icon'          => 'dashicons-location',
								'query_var'          => true,
								'rewrite'            => array( 'slug' => 'localizacao' ),
								'capability_type'    => 'post',
								'has_archive'        => true,
								'hierarchical'       => false,
								'supports'           => array( 'title', 'thumbnail' )
							);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('localizacao', $argsLocalizacao);

	}

	// CUSTOM POST TYPE TIME
	function tipoTime() {

		$rotulosTime = array(
								'name'               => 'Time',
								'singular_name'      => 'time',
								'menu_name'          => 'Time',
								'name_admin_bar'     => 'Time',
								'add_new'            => 'Adicionar novo',
								'add_new_item'       => 'Adicionar novo time',
								'new_item'           => 'Novo time',
								'edit_item'          => 'Editar time',
								'view_item'          => 'Ver time',
								'all_items'          => 'Todos os times',
								'search_items'       => 'Buscar time',
								'parent_item_colon'  => 'Dos times',
								'not_found'          => 'Nenhum time cadastrado.',
								'not_found_in_trash' => 'Nenhum time na lixeira.'
							);

		$argsTime 	= array(
								'labels'             => $rotulosTime,
								'public'             => true,
								'publicly_queryable' => true,
								'show_ui'            => true,
								'show_in_menu'       => true,
								'menu_position'		 => 9,
								'menu_icon'          => 'dashicons-groups',
								'query_var'          => true,
								'rewrite'            => array( 'slug' => 'time' ),
								'capability_type'    => 'post',
								'has_archive'        => true,
								'hierarchical'       => false,
								'supports'           => array( 'title','thumbnail' )
							);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('time', $argsTime);

	}


	/****************************************************
	* TAXONOMIA
	*****************************************************/
	function taxonomiaStardente () {		
		taxonomiaCategoriaDestaque();
		taxonomiaTratamento();
	}
		// TAXONOMIA DE DESTAQUE
		function taxonomiaCategoriaDestaque() {

			$rotulosCategoriaDestaque = array(
				'name'              => 'Categorias de destaque',
				'singular_name'     => 'Categoria de destaque',
				'search_items'      => 'Buscar categorias de destaque',
				'all_items'         => 'Todas as categorias de destaque',
				'parent_item'       => 'Categoria de destaque pai',
				'parent_item_colon' => 'Categoria de destaque pai:',
				'edit_item'         => 'Editar categoria de destaque',
				'update_item'       => 'Atualizar categoria de destaque',
				'add_new_item'      => 'Nova categoria de destaque',
				'new_item_name'     => 'Nova categoria',
				'menu_name'         => 'Categorias de destaque',
				);

			$argsCategoriaDestaque 		= array(
				'hierarchical'      => true,
				'labels'            => $rotulosCategoriaDestaque,
				'show_ui'           => true,
				'show_admin_column' => true,
				'query_var'         => true,
				'rewrite'           => array( 'slug' => 'categoria-destaque' ),
				);

			register_taxonomy( 'categoriaDestaque', array( 'destaque' ), $argsCategoriaDestaque );

		}

		function taxonomiaTratamento() {

			$rotulosCategoriaTratamento = array(
				'name'              => 'Categorias de tratamento',
				'singular_name'     => 'Categoria de tratamento',
				'search_items'      => 'Buscar categorias de tratamento',
				'all_items'         => 'Todas as categorias de tratamento',
				'parent_item'       => 'Categoria de tratamento pai',
				'parent_item_colon' => 'Categoria de tratamento pai:',
				'edit_item'         => 'Editar categoria de tratamento',
				'update_item'       => 'Atualizar categoria de tratamento',
				'add_new_item'      => 'Nova categoria de tratamento',
				'new_item_name'     => 'Nova categoria',
				'menu_name'         => 'Categorias de tratamento',
				);

			$argsCategoriaTratamento 		= array(
				'hierarchical'      => true,
				'labels'            => $rotulosCategoriaTratamento,
				'show_ui'           => true,
				'show_admin_column' => true,
				'query_var'         => true,
				'rewrite'           => array( 'slug' => 'categoria-tratamento' ),
				);

			register_taxonomy( 'categoriaTratamento', array( 'tratamento' ), $argsCategoriaTratamento );

		}			

    /****************************************************
	* META BOXES
	*****************************************************/
	function metaboxesStardente(){

		add_filter( 'rwmb_meta_boxes', 'registraMetaboxes' );

	}

		function registraMetaboxes( $metaboxes ){

			$prefix = 'Stardente_';

			// METABOX DE DESTAQUE
			$metaboxes[] = array(

				'id'			=> 'detalhesMetaboxDestaqueInicial',
				'title'			=> 'Detalhes do Destaque',
				'pages' 		=> array( 'destaque-inicial' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(

					array(
						'name'  => 'Foto Mobile: ',
						'id'    => "{$prefix}foto_mob",
						'desc'  => 'Foto mobile 320 X 480',
						'type'  => 'image_advanced',
						'max_file_uploads' => 1,
					),
									
				),
			);
			// METABOX DE TRATAMENTO CLÍNICO
			$metaboxes[] = array(

				'id'			=> 'detalhesMetaboxTratamentoClinico',
				'title'			=> 'Detalhes do tratamento clínico',
				'pages' 		=> array( 'tratamento' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(
					array(
						'name'  => 'Ícone branco: ',
						'id'    => "{$prefix}icone_tratamento_clinico",
						'desc'  => '',
						'type'  => 'image_advanced',
						'max_file_uploads' => 1,
					),
					array(
						'name'  => 'Resumo: ',
						'id'    => "{$prefix}resumo_tratamento",
						'desc'  => '',
						'type'  => 'textarea',
					),	
				),
			);
			// METABOX DE TIME
			$metaboxes[] = array(

				'id'			=> 'detalhesMetaboxTime',
				'title'			=> 'Detalhes do Time',
				'pages' 		=> array( 'time' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(

					array(
						'name'  => 'Sobre o time: ',
						'id'    => "{$prefix}sobre_time",
						'desc'  => '',
						'type'    => 'fieldset_text',

					    // Options: array of key => Label for text boxes
					    // Note: key is used as key of array of values stored in the database
						'options' => array(
							'texto'    => 'Info',
						),

					    // Is field cloneable?
						'clone' => true,
					),
									
				),
			);
			// METABOX DE LOCALIZAÇÃO
			$metaboxes[] = array(

				'id'			=> 'detalhesMetaboxLocalizacao',
				'title'			=> 'Detalhes da localização',
				'pages' 		=> array( 'localizacao' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(

					array(
						'name'  => 'Endereço: ',
						'id'    => "{$prefix}localizacao_endereco",
						'desc'  => '',
						'type'    => 'text',
					),
					array(
						'name'  => 'Telefone: ',
						'id'    => "{$prefix}localizacao_telefone",
						'desc'  => '',
						'type'    => 'text',
					),
					array(
						'name'  => 'Celular: ',
						'id'    => "{$prefix}localizacao_celular",
						'desc'  => '',
						'type'    => 'text',
					),
									
				),
			);
			// METABOX DE LOCALIZAÇÃO
			$metaboxes[] = array(

				'id'			=> 'detalhesMetaboxLocalizacaoEndereco',
				'title'			=> 'Detalhes do endereço',
				'pages' 		=> array( 'localizacao' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(

					array(
						'name'  => 'Link endereço: ',
						'id'    => "{$prefix}localizacao_link_endereco",
						'desc'  => '',
						'type'    => 'text',
					),
									
				),
			);
			// METABOX DE LOCALIZAÇÃO
			$metaboxes[] = array(

				'id'			=> 'detalhesMetaboxLocalizacaoFormulario',
				'title'			=> 'Formulário de agendamento localização',
				'pages' 		=> array( 'localizacao' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(

					array(
						'name'  => 'Shortcode formulário: ',
						'id'    => "{$prefix}localizacao_formulario",
						'desc'  => '"[contact-form-7 id="81" title="Agendamento Araucária Centro"]"',
						'type'    => 'text',
					),
					array(
						'name'  => 'Imagem formulário: ',
						'id'    => "{$prefix}imagem_mapa_formulario",
						'desc'  => '',
						'type'  => 'image_advanced',
						'max_file_uploads' => 1,
					),
									
				),
			);
			// METABOX DE DEPOIMENTOS
			$metaboxes[] = array(

				'id'			=> 'detalhesMetaboxDepoimentos',
				'title'			=> 'Detalhes do depoimento',
				'pages' 		=> array( 'depoimento' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(

					array(
						'name'  => 'Cargo: ',
						'id'    => "{$prefix}depoimento_cargo",
						'desc'  => '',
						'type'    => 'text',
					),
					array(
						'name'  => 'Link instagram: ',
						'id'    => "{$prefix}depoimentos_link_instagram",
						'desc'  => '',
						'type'    => 'text',
					),
									
				),
			);

			return $metaboxes;
		}

		function metaboxjs(){

			global $post;
			$template = get_post_meta($post->ID, '_wp_page_template', true);
			$template = explode('/', $template);
			$template = explode('.', $template[1]);
			$template = $template[0];

			if($template != ''){
				wp_enqueue_script( 'metabox-js', get_template_directory_uri() 	. '/js/metabox_' . $template . '.js', array('jquery'), '1.0', true);
			}
		}

	/****************************************************
	* SHORTCODES
	*****************************************************/
	function shortcodesStardente(){

	}

	/****************************************************
	* ATALHOS VISUAL COMPOSER
	*****************************************************/
	function visualcomposerStardente(){

	    if (class_exists('WPBakeryVisualComposer')){

		}

	}

  	/****************************************************
	* AÇÕES
	*****************************************************/

	// INICIA A FUNÇÃO PRINCIPAL
	add_action('init', 'baseStardente');

	// IMPLEMENTAÇÃO ADICIONAL PARA EXIBIR/OCULTAR META BOX DE PÁGINAS SIMPLES
	add_action( 'add_meta_boxes', 'metaboxjs');

	// FLUSHS
	function rewrite_flush() {

    	baseStardente();

   		flush_rewrite_rules();
	}

	register_activation_hook( __FILE__, 'rewrite_flush' );