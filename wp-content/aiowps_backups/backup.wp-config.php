<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'stardent_site' );

/** MySQL database username */
define( 'DB_USER', 'stardent_root' );

/** MySQL database password */
define( 'DB_PASSWORD', 's@pp1705' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'd,eN~P[C~+$Vnz:~;Wgx]n[y%:&3fzY@N!$&n^=6`7F}P%>!8wyCx[(u)cg;,Bvz' );
define( 'SECURE_AUTH_KEY',  ' n% W#rFM!{!2[r,v&p81_F+3z0T}9MOC>$QvCq6p+I|*RyF6{^#YW&99z}W<BCc' );
define( 'LOGGED_IN_KEY',    'W;><J{,sN_*=J:Ir/U9uO&=hHram72NM7;Z70TA.Y]mcXTC@^W;.oc|5^su8uQtx' );
define( 'NONCE_KEY',        'Jx9j-MLoWkrnGlQO`a9S|@<t&H%f=YqY>SGW6RVvtrCzYilb$>`nH7UsYl.M_m>7' );
define( 'AUTH_SALT',        'O:WjK/*ZG:,Ic0tqU*jB|NRsOeR,u>Nq0/jL1)_yiS;B#H<RwK<R;;zi_AGV#$<F' );
define( 'SECURE_AUTH_SALT', '<o6vX1_Aic1(:i*^oojL+#4UG~_6an).~^pc5o=PP8{3o`s-Ijpdu[HoXxabu_X:' );
define( 'LOGGED_IN_SALT',   'F}00<eWUSqpIq. W|#UYAn)iafQ3;`P<|%-8^d/OrVls]ajfDFP_+,?XJX`oRGD0' );
define( 'NONCE_SALT',       ':p(5F(mw1z>YWya3Oi/nMyRKK00.mn(Jk}<nJ]$+wJ(J%w{*TJ8:17y4{e--Sf4W' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'st_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
