(function(){

	$(document).ready(function(){

		$('.carrossel-destaque').owlCarousel({
			items : 1,
			dots: true,
			nav: false,
			loop: true,
			lazyLoad: false,
			mouseDrag: false,
			touchDrag: true,	
			smartSpeed: 450,
			center: false,
			margin: 0,
			animateOut: 'fadeOut',
			autoplay: true,
			autoplayTimeout: 3000,
		});

		// $('.carrossel-destaque').owlCarousel({
		// 	items: 1,
		// 	dots: true,
		// 	nav: true,
		// 	loop: false,
		// 	lazyLoad: false,
		// 	mouseDrag: false,
		// 	touchDrag: true,	
		// 	smartSpeed: 450,
		// 	singleItem : true,
		// 	animateOut: 'fadeOut',
		// });

		$('.carrossel-imagem').owlCarousel({
			items : 1,
			dots: false,
			loop: true,
			lazyLoad: false,
			mouseDrag: false,
			touchDrag: false,	
			smartSpeed: 450,
			center: false,
			margin: 0,
			animateOut: 'fadeOut',
			autoplay: true,
			autoplayTimeout: 3000,
		});

		$('.carrossel-depoimentos').owlCarousel({
			items : 4,
			dots: true,
			loop: true,
			lazyLoad: false,
			mouseDrag: true,
			touchDrag: true,	
			smartSpeed: 450,
			center: true,
			margin: 45,
			responsiveClass:true,			    
			responsive:{
				320:{
					items:1,
					margin: 15,
				},
				425:{
					items:2,
					margin: 15,
				},
				500:{
					items:2,
					margin: 15,
				},
				768:{
					items:3,
					margin: 30,
				},
				850:{
					items:4,
				},
			}
		});

		$('.carousel').carousel();
		$('.button-next').click(function(){
			$('.carousel').carousel('next');
		});
		$('.button-prev').click(function(){
			$('.carousel').carousel('prev');
		});

		let isClicked = false;
		$('header .navbar .navbar-toggler').click(function(){
			if(!isClicked){
				$('header .navbar .navbar-toggler .navbar-toggler-icon img').attr('src', 'http://stardente.gran.group/wp-content/uploads/2020/01/x.svg');
				isClicked = true;
			} else{
				$('header .navbar .navbar-toggler .navbar-toggler-icon img').attr('src', 'http://stardente.gran.group/wp-content/uploads/2020/01/menu.svg');
				isClicked = false;
			}
		});

		// $('.pg-inicial .secao-tratamentos-clinicos ul li a').click(function(){
		// 	let dataTitle = $(this).parent().attr('data-title');
		// 	let dataText = $(this).parent().attr('data-text');
		// 	let dataIcon = $(this).parent().attr('data-icon');

		// 	$('.pg-inicial .secao-tratamentos-clinicos ul li').removeClass('tratamento-active');
		// 	$(this).parent().addClass('tratamento-active');

		// 	$('.pg-inicial .secao-tratamentos-clinicos article .titulo').text(dataTitle);
		// 	$('.pg-inicial .secao-tratamentos-clinicos article p').text(dataText);
		// 	$('.pg-inicial .secao-tratamentos-clinicos article .icone-tratamento').attr('src', dataIcon);
		// });

		$('.pg-inicial .secao-tratamentos-clinicos ul li').click(function(){
			let dataTitle = $(this).attr('data-title');
			let dataLink = $(this).attr('data-link');
			let dataText = $(this).attr('data-text');
			let dataIcon = $(this).attr('data-icon');

			$('.pg-inicial .secao-tratamentos-clinicos ul li').removeClass('tratamento-active');
			$(this).addClass('tratamento-active');

			$('.pg-inicial .secao-tratamentos-clinicos article .titulo').text(dataTitle);
			$('.pg-inicial .secao-tratamentos-clinicos article .button-ler-mais').attr('href', dataLink);
			$('.pg-inicial .secao-tratamentos-clinicos article p').text(dataText);
			$('.pg-inicial .secao-tratamentos-clinicos article .icone-tratamento').attr('src', dataIcon);
		});
		$('.pg-inicial .secao-tratamentos-clinicos ul li[data-title="Restauração"]').click();

		$('span.button-agendar').click(function(){
			$('.pop-up-agendamento').addClass('pop-up-agendamento-active');
			$('body').addClass('remover-scroll');
		});
		$('.pop-up-agendamento .pop-up .area-formulario .info-localizacao .fechar-pop-up').click(function(){
			$('.pop-up-agendamento').removeClass('pop-up-agendamento-active');
			$('body').removeClass('remover-scroll');
		});
		$('.pop-up-agendamento .pop-up .area-formulario .formulario-agendamento form .button-cancelar .span-cancelar').click(function(){
			$('.pop-up-agendamento').removeClass('pop-up-agendamento-active');
			$('body').removeClass('remover-scroll');
		});

		$('.pop-up-agendamento .pop-up .agende ul li').click(function(){
			let dataId = $(this).attr('data-id');

			$('.pop-up-agendamento .pop-up .agende ul li').removeClass('clinica-active');
			$(this).addClass('clinica-active');

			$('.pop-up-agendamento .pop-up .area-formulario').removeClass('formulario-active');
			$('.pop-up-agendamento .pop-up .area-formulario#'+dataId).addClass('formulario-active');

			$('.pop-up-agendamento .pop-up .mapa').removeClass('mapa-active');
			$('.pop-up-agendamento .pop-up #'+dataId+'.mapa').addClass('mapa-active');
		});

		// $('.pg-inicial .secao-tratamentos-esteticos article ul li').click(function(){
		// 	$('.pop-up-agendamento').addClass('pop-up-agendamento-active');
		// 	$('body').addClass('remover-scroll');
		// });

		$('header .navbar .navbar-collapse .nav-item a').addClass('nav-link');

		$('li.scrollTop a').click(function() {
			if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
				var target = $(this.hash);
				target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
				if (target.length) {
					$('html,body').animate({
						scrollTop: target.offset().top
					}, 1000);
					return false;
				}
			}
		});

		$('a.scrollTop').click(function() {
			if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
				var target = $(this.hash);
				target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
				if (target.length) {
					$('html,body').animate({
						scrollTop: target.offset().top
					}, 1000);
					return false;
				}
			}
		});

		// if(screen.width > 768){
		// 	$('.pg-inicial .secao-tratamentos-clinicos ul li a').click(function(e){
		// 		e.preventDefault();
		// 	});
		// }		

		if(screen.width <= 768){
			let carousel_itens = $('.pg-inicial .secao-time .carrossel-time .carousel').children();
			$('.pg-inicial .secao-time .carrossel-time .carousel').remove();

			$('.pg-inicial .secao-time .carrossel-time').append(carousel_itens);
			$('.pg-inicial .secao-time .carrossel-time').addClass('owl-carousel owl-theme');


			$('.carrossel-time').owlCarousel({
				items : 1,
				dots: false,
				nav: true,
				loop: true,
				lazyLoad: false,
				mouseDrag: true,
				touchDrag: true,	
				smartSpeed: 450,
				center: false,
				margin: 0,
				autoplay: false,
				animateOut: 'fadeOut',
				// responsiveClass:true,
				// responsive:{
				// 	320:{
				// 		items:1,

				// 	},
				// 	425:{
				// 		items:3,

				// 	},
				// 	768:{
				// 		items:4,

				// 	},
				// 	991:{
				// 		items:5,

				// 	},
				// }
			});

			$('header .navbar .navbar-collapse .nav-item .nav-link').click(function(){
				$('header .navbar .navbar-toggler').click();
			});
		}

		$('.pop-up-agendamento .pop-up .area-formulario .formulario-agendamento form input.wpcf7-text').focus(function(){
			$(this).attr('placeholder', '');
		});

		$('.pop-up-agendamento .pop-up .area-formulario .formulario-agendamento form input.wpcf7-text').focusout(function(){
			$('.pop-up-agendamento .pop-up .area-formulario .formulario-agendamento form input.agendamento-nome-completo').attr('placeholder', 'Nome completo');
			$('.pop-up-agendamento .pop-up .area-formulario .formulario-agendamento form input.agendamento-email').attr('placeholder', 'E-mail');
			$('.pop-up-agendamento .pop-up .area-formulario .formulario-agendamento form input.agendamento-celular').attr('placeholder', 'Celular/Whats(com DDD)');
		});

	});

}());