<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Stardente
 */

$loopPost = new WP_Query( array( 'post_type' => 'post', 'orderby' => 'id', 'order' => 'desc', 'posts_per_page' => 3) );

get_header();
?>

<div class="pg pg-blog">
	<!-- <section class="secao-destaque">
		<h4 class="hidden">SEÇÃO DESTAQUE</h4>
		<div class="carrossel-destaque destaque carrossel-destaque-menor carrossel-destaque-blog owl-carousel owl-theme">

			<?php while ( $loopPost->have_posts() ) : $loopPost->the_post(); 
				$categoriasPost = get_the_category();
				foreach ($categoriasPost as $categoria): ?>
			<div class="item" style="background-image: url(<?php echo wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' )[0] ?>);">
				<div class="container">
					<article>
						<a href="<?php echo get_category_link($categoria->cat_ID); ?>" class="link-categoria"><?php echo $categoria->name; ?></a>
						<h2 class="titulo"><?php echo get_the_title() ?></h2>
						<a href="<?php echo get_permalink() ?>" class="button-padrao">Ler mais</a>
					</article>
				</div>
			</div>
			<?php endforeach; endwhile; wp_reset_query(); ?>

		</div>
	</section> -->
	<section class="secao-posts">
		<h4 class="hidden">SEÇÃO POSTS</h4>
		<div class="full-container">
			<?php
			the_archive_title( '<h2 class="page-title titulo">', '</h2>' );
			the_archive_description( '<div class="archive-description">', '</div>' );
			?>
			<ul class="lista-posts">
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); 
					$categoriasPost = get_the_category();
					foreach ($categoriasPost as $categoria): ?>
				<li>
					<a href="<?php echo get_permalink() ?>" class="link-imagem">
						<figure>
							<img src="<?php echo wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' )[0] ?>" alt="<?php echo get_the_title() ?>">
							<figcaption class="hidden"><?php echo get_the_title() ?></figcaption>
						</figure>
					</a>
					<div class="post-content">
						<a href="<?php echo get_category_link($categoria->cat_ID); ?>" class="link-categoria"><?php echo $categoria->name; ?></a>
						<a href="<?php echo get_permalink() ?>" class="link-titulo">
							<h2 class="titulo-post"><?php echo get_the_title() ?></h2>
						</a>
						<span class="data"><?php the_time('j M Y'); ?></span>
					</div>
				</li>
				<?php endforeach; endwhile; endif; ?>
			</ul>
			<div class="paginador">
				<?php 
				if(function_exists('pagination')){
					pagination($additional_loop->$max_num_pages);
				}
				?>
			</div>
		</div>
	</section>
</div>

<?php get_footer();