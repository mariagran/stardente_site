<?php
/**
 * Template Name: Inicial 
 * Description: Página Inicial
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Stardente
 */

$destaques = new WP_Query(array( 'post_type' => 'destaque', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1 ));
$depoimentos = new WP_Query(array( 'post_type' => 'depoimento', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1 ));
$localizacoes = new WP_Query(array( 'post_type' => 'localizacao', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1 ));
$time = new WP_Query(array( 'post_type' => 'time', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1 ));

$tratamentos_esteticos = new WP_Query(
	array(
		'post_type' => 'tratamento',
		'posts_per_page' => -1, 
		'orderby' => 'id',
		'tax_query' => array(
			array(
				'taxonomy' => "categoriaTratamento",
				'field'    => 'slug',
				'terms'    => "estetica",
			)
		)
	)
);
							
$tratamentos_clinicos = new WP_Query(
	array(
		'post_type' => 'tratamento',
		'posts_per_page' => -1, 
		'orderby' => 'id',
		'tax_query' => array(
			array(
				'taxonomy' => "categoriaTratamento",
				'field'    => 'slug',
				'terms'    => "odontologia",
			)
		)
	)
);

get_header(); 

?>

<main class="pg pg-inicial">
	<section class="secao-destaque">
		<h4 class="hidden">Seção destaque</h4>
		<div class="carrossel-destaque owl-carousel owl-theme">

			<?php
			while($destaques->have_posts()): $destaques->the_post();
				$imagem_destaque = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' )[0];
			?>
			<div class="item" style="background-image: url(<?php echo $imagem_destaque; ?>);">
				<div class="large-container">
					<figure class="hidden">
						<img src="<?php echo $imagem_destaque; ?>" alt="<?php echo get_the_title(); ?>">
						<figcaption><?php echo get_the_title(); ?></figcaption>
					</figure>
					<article>
						<h2 class="titulo primario"><?php echo get_the_title(); ?></h2>
						<p><?php echo get_the_content(); ?></p>
						<span class="button-agendar">Agendar consulta</span>
					</article>
				</div>
			</div>
			<?php endwhile; ?>

		</div>
	</section>

	<section id="secao-sobre" class="secao-sobre">
		<h4 class="hidden">Seção sobre a clínica</h4>
		<div class="large-container">
			<div class="row">
				<div class="col-md-6 col-desktop">
					<div class="carrossel-imagem carrossel-sobre owl-carousel owl-theme">
						
						<?php
						$imagens_sobre = explode(",", $configuracao['opt_galeria_secao_sobre']);
						foreach ($imagens_sobre as $imagem_sobre):
							$url_imagem_sobre = wp_get_attachment_image_url($imagem_sobre, 'full');
						?>
						<figure>
							<img src="<?php echo $url_imagem_sobre ?>" alt="Imagem sobre">
							<figcaption class="hidden">Imagem sobre</figcaption>
						</figure>
						<?php endforeach; ?>

					</div>
				</div>
				<div class="col-md-6">
					<article>
						<h2 class="titulo secundario"><?php echo $configuracao['opt_sobre_titulo']; ?></h2>
						<p><?php echo $configuracao['opt_sobre_descricao']; ?></p>
						<ul>

							<?php
							$imagem_planos = explode(",", $configuracao['opt_planos']);
							$contadorPlanos = 0;
							foreach ($imagem_planos as $plano):
								$url_imagem_plano = wp_get_attachment_image_url($plano);
							?>
							<li><a href="<?php echo $configuracao['opt_planos_link'][$contadorPlanos] ?>" target="_blank"><img src="<?php echo $url_imagem_plano; ?>" alt="Plano odontológico"></a></li>
							<?php $contadorPlanos++; endforeach; ?>

						</ul>
					</article>
				</div>
				<div class="col-md-6 col-mobile">
					<div class="carrossel-imagem carrossel-sobre owl-carousel owl-theme">
						
						<?php
						$imagens_sobre = explode(",", $configuracao['opt_galeria_secao_sobre']);
						foreach ($imagens_sobre as $imagem_sobre):
							$url_imagem_sobre = wp_get_attachment_image_url($imagem_sobre, 'full');
						?>
						<figure>
							<img src="<?php echo $url_imagem_sobre ?>" alt="Imagem sobre">
							<figcaption class="hidden">Imagem sobre</figcaption>
						</figure>
						<?php endforeach; ?>

					</div>
				</div>
			</div>
		</div>
	</section>

	<section id="secao-tratamentos-clinicos" class="secao-tratamentos-clinicos">
		<h4 class="hidden">Seção de tratamentos clínicos</h4>
		<div class="mid-container">
			<div class="row">
				<div class="col-md-7 col-cinza col-desktop">
					<article>
						<img src="<?php echo get_template_directory_uri() . '/img/aparelho1.png'; ?>" alt="Ícone tratamento" class="icone-tratamento">
						<h2 class="titulo secundario">Aparelho Ortodôntico</h2>
						<p>Sum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting.</p>
						<div class="div-button-ler-mais">
							<a href="#" target="_blank" class="button-ler-mais button-agendar">Ler mais</a>
						</div>
						<div class="div-button-agendar">
							<span class="button-agendar">Agendar consulta</span>
						</div>
					</article>
				</div>
				<div class="col-md-5 col-roxo">
					<ul>

						<?php
						while($tratamentos_clinicos->have_posts()): $tratamentos_clinicos->the_post();
							$icone_destaque = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' )[0];
							$icone = rwmb_meta('Stardente_icone_tratamento_clinico', array('size' => 'thumbnail'));
							$resumo = rwmb_meta('Stardente_resumo_tratamento');
						?>
						<li data-title="<?php echo get_the_title(); ?>" data-link="<?php echo get_permalink(); ?>" data-text="<?php echo $resumo; ?>" data-icon="<?php echo $icone_destaque; ?>">
							<!-- <a href="#conteudo-tratamendo-clinico" class="scrollTop"> -->

								<?php foreach($icone as $icone): ?>
									<img src="<?php echo $icone['url']; ?>" alt="<?php echo get_the_title(); ?>">
								<?php endforeach; ?>

								<p><?php echo get_the_title(); ?></p>
							<!-- </a> -->
						</li>
						<?php endwhile; wp_reset_query(); ?>

					</ul>
				</div>
				<div id="conteudo-tratamendo-clinico" class="col-md-7 col-cinza col-mobile">
					<article>
						<img src="<?php echo get_template_directory_uri() . '/img/aparelho1.png'; ?>" alt="Ícone tratamento" class="icone-tratamento">
						<h2 class="titulo secundario">Aparelho Ortodôntico</h2>
						<p>Sum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting.</p>
						<div class="div-button-ler-mais">
							<span class="button-ler-mais button-agendar">Ler mais</span>
						</div>
						<div class="div-button-agendar">
							<span class="button-agendar">Agendar consulta</span>
						</div>
					</article>
				</div>
			</div>
		</div>
	</section>

	<section id="secao-tratamentos-esteticos" class="secao-tratamentos-esteticos">
		<h4 class="hidden">Seção de tratamentos estéticos</h4>
		<div class="full-container">
			<div class="row">
				<div class="col-md-7">
					<div class="carrossel-imagem carrossel-tratamento owl-carousel owl-theme">
						
						<?php
						$imagens_tratamentos_esteticos = explode(",", $configuracao['opt_galeria_secao_tratamentos_esteticos']);
						foreach ($imagens_tratamentos_esteticos as $imagem_tratamento_esteticos):
							$url_imagem_tratamento_esteticos = wp_get_attachment_image_url($imagem_tratamento_esteticos, 'full');
						?>
						<figure>
							<img src="<?php echo $url_imagem_tratamento_esteticos ?>" alt="Imagem tratamento estético">
							<figcaption class="hidden">Imagem sobre</figcaption>
						</figure>
						<?php endforeach; ?>

					</div>
				</div>
				<div class="col-md-5">
					<article>
						<h2 class="titulo secundario"><?php echo $configuracao['opt_tratamentos_esteticos_titulo']; ?></h2>
						<?php echo $configuracao['opt_tratamentos_esteticos_descricao']; ?>
						<ul>
							
							<?php while($tratamentos_esteticos->have_posts()): $tratamentos_esteticos->the_post(); ?>
							<li><a href="<?php echo get_permalink();?>"><?php echo get_the_title(); ?></a></li>
							<?php endwhile; ?>

						</ul>
						<span class="button-agendar">Agendar consulta</span>
					</article>
				</div>
			</div>
		</div>
	</section>

	<section id="secao-depoimentos" class="secao-depoimentos">
		<h4 class="hidden">Seção de depoimentos</h4>
		<div class="large-container">
			<article>
				<h2 class="titulo"><?php echo $configuracao['opt_depoimentos_titulo'] ?></h2>
				<p><?php echo $configuracao['opt_depoimentos_descricao'] ?></p>
			</article>
		</div>
		<div class="full-container">
			<ul class="carrossel-depoimentos owl-carousel owl-theme">
				
				<?php
				while($depoimentos->have_posts()): $depoimentos->the_post();
					$depoimento_foto = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full')[0];
				?>
				<li>
					<img src="<?php echo $depoimento_foto; ?>" alt="<?php echo get_the_title(); ?>" class="foto-depoimento">
					<h3 class="titulo"><?php echo get_the_title(); ?></h3>
					<span class="cargo"><?php echo rwmb_meta('Stardente_depoimento_cargo'); ?></span>
					<p><?php echo get_the_content(); ?></p>
					<a href="<?php rwmb_meta('Stardente_depoimentos_link_instagram'); ?>"><img src="<?php echo get_template_directory_uri() . '/img/instagram.png'; ?>" alt="Instagram depoimento: <?php echo rwmb_meta('Stardente_depoimentos_link_instagram'); ?>"></a>
				</li>
				<?php endwhile; ?>

			</ul>
		</div>
	</section>

	<section id="secao-localizacao" class="secao-localizacao">
		<h4 class="hidden">Seção de localização</h4>
		<div class="large-container">
			<div class="row">
				<div class="col-md-6 col-mobile">
					<article>
						<img src="<?php echo $configuracao['opt_localizacao_icone']['url']; ?>" alt="Ícone localização" class="icone-localizacao">
						<h2 class="titulo secundario"><?php echo $configuracao['opt_localizacao_titulo']; ?></h2>
						<?php echo $configuracao['opt_localizacao_descricao']; ?>
					</article>
				</div>
				<div class="col-md-6">
					<ul>

						<?php
						while($localizacoes->have_posts()): $localizacoes->the_post();
							$imagem_mapa = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full')[0];
						?>
						<li>
							<div class="info-localizacao">
								<h3 class="titulo"><?php echo get_the_title(); ?></h3>
								<p><?php echo rwmb_meta('Stardente_localizacao_endereco'); ?></p>
								<span class="telefone tel"><?php echo rwmb_meta('Stardente_localizacao_telefone'); ?></span>
								<span class="telefone cel"><?php echo rwmb_meta('Stardente_localizacao_celular'); ?></span>
							</div>
							<figure class="mapa">
								<img src="<?php echo $imagem_mapa; ?>" alt="<?php echo rwmb_meta('Stardente_localizacao_endereco'); ?>">
								<figcaption class="hidden"><?php echo rwmb_meta('Stardente_localizacao_endereco'); ?></figcaption>
							</figure>
						</li>
						<?php endwhile; ?>

					</ul>
				</div>
				<div class="col-md-6 col-desktop">
					<article>
						<img src="<?php echo $configuracao['opt_localizacao_icone']['url']; ?>" alt="Ícone localização" class="icone-localizacao">
						<h2 class="titulo secundario"><?php echo $configuracao['opt_localizacao_titulo']; ?></h2>
						<?php echo $configuracao['opt_localizacao_descricao']; ?>
					</article>
				</div>
			</div>
		</div>
	</section>

	<section id="secao-time" class="secao-time">
		<h4 class="hidden">Seção time</h4>
		<div class="large-container">
			<div class="titulo-time">
				<span><?php echo $configuracao['opt_time_titulo_1'] ?></span>
				<h3 class="titulo terciario"><?php echo $configuracao['opt_time_titulo_2'] ?></h3>
				<div class="carrossel-nav">
					<button class="button-prev">
						<img src="<?php echo get_template_directory_uri() . '/img/left.svg'; ?>" alt="Ícone botão">
					</button>
					<button class="button-next">
						<img src="<?php echo get_template_directory_uri() . '/img/right.svg'; ?>" alt="Ícone botão">
					</button>
				</div>
			</div>
			<div class="carrossel-time">
				<div class="carousel">

					<?php
					while($time->have_posts()): $time->the_post();
						$titulo = explode('. ', get_the_title());
						$foto_time = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full')[0];
						$infos = rwmb_meta('Stardente_sobre_time');
					?>
					<a class="carousel-item" href="#">
						<figure>
							<img src="<?php echo $foto_time; ?>" alt="<?php echo get_the_title(); ?>" class="foto-time">
							<figcaption class="hidden"><?php echo get_the_title(); ?></figcaption>
						</figure>
						<article class="sobre-time">
							<h3 class="titulo"><span><?php echo $titulo[0] ?>.</span> <?php echo $titulo[1]; ?></h3>
							<ul>

								<?php foreach ($infos as $info): ?>
								<li><?php echo $info['texto']; ?></li>
								<?php endforeach; ?>

							</ul>
						</article>
					</a>
					<?php endwhile; ?>

				</div>
			</div>
		</div>
	</section>

	<div id="agende-sua-consulta" class="agende-sua-consulta">
		<div class="large-container">
			<div class="row">
				<div class="col-md-6">
					<article>
						<h3 class="titulo terciario"><?php echo $configuracao['opt_agende_consulta_titulo']; ?></h3>
						<p><?php echo $configuracao['opt_agende_consulta_descricao']; ?></p>
						<span class="button-agendar">Agendar consulta</span>
					</article>
				</div>
				<div class="col-md-6">
					<div class="planos">
						<h2 class="titulo"><?php echo $configuracao['opt_planos_titulo']; ?></h2>
						<ul>

							<?php
							$contador = 0;
							$imagem_planos = explode(",", $configuracao['opt_planos']);
							foreach ($imagem_planos as $plano):
								$url_imagem_plano = wp_get_attachment_image_url($plano);
								if($contador == 0){
									$classeCss = "";
								} elseif($contador == 1){
									$classeCss = 'text-center';
								} else{
									$classeCss = 'text-right';
								}
							?>
							<li class="<?php echo $classeCss; ?>"><a href="<?php echo $configuracao['opt_planos_link'][$contador] ?>" target="_blank"><img src="<?php echo $url_imagem_plano; ?>" alt="Plano odontológico"></a></li>
							<?php $contador++; endforeach; ?>

						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="pop-up-agendamento">
		<div class="pop-up">
			<div class="row">
				<div class="col-md-4">
					<div class="agende">
						<img src="<?php echo get_template_directory_uri() . '/img/calendar (2)@2x.png'; ?>" alt="Ícone agendamento" class="icone-agendamento">
						<h3 class="titulo">Agende <br>sua Consulta</h3>
						<p>Lorem ipsum is simply dummy text of the printing and typesetting industry.</p>
						<span>Selecione uma de nossas clínicas:</span>
						<ul>
							<?php
								$contador = 0;
								while($localizacoes->have_posts()): $localizacoes->the_post();
									if($contador == 0){
										$classeCssClinica = "clinica-active";
									} else{
										$classeCssClinica = "";
									}
							?>
							<li data-id="<?php echo $contador; ?>" class="<?php echo $classeCssClinica ?>"><h3><?php echo get_the_title(); ?></h3></li>
							<?php $contador++; endwhile; wp_reset_query(); ?>
						</ul>
					</div>
				</div>
				<div class="col-md-6">
					<?php
						$contador2 = 0;
						while($localizacoes->have_posts()): $localizacoes->the_post();
							if($contador2 == 0){
								$classeCssFormulario = "formulario-active";
							} else{
								$classeCssFormulario = "";
							}
					?>
					<div id="<?php echo $contador2; ?>" class="area-formulario <?php echo $classeCssFormulario; ?>">
						<div class="info-localizacao">
							<span class="fechar-pop-up"><img src="<?php echo get_template_directory_uri() . '/img/Combined Shape-1@3x.png'; ?>" alt="Ícone fechar pop up"></span>
							<h3 class="titulo"><?php echo get_the_title(); ?></h3>
							<p><?php echo rwmb_meta('Stardente_localizacao_endereco'); ?></p>
							<span class="telefone tel"><?php echo rwmb_meta('Stardente_localizacao_telefone'); ?></span>
							<span class="telefone cel"><?php echo rwmb_meta('Stardente_localizacao_celular'); ?></span>
						</div>
						<div class="formulario-agendamento">
							<?php echo do_shortcode("'" . rwmb_meta('Stardente_localizacao_formulario') . "'"); ?>
						</div>
					</div>
					<?php $contador2++; endwhile; wp_reset_query(); ?>
				</div>
				<div class="col-md-2">
					<?php
						$contador3 = 0;
						while($localizacoes->have_posts()): $localizacoes->the_post();
							if($contador3 == 0){
								$classeCssMapa = "mapa-active";
							} else{
								$classeCssMapa = "";
							}
							$icone = rwmb_meta('Stardente_imagem_mapa_formulario', array('size' => 'full'));
							foreach($icone as $icone):
					?>
					<figure id="<?php echo $contador3; ?>" class="mapa <?php echo $classeCssMapa; ?>">
						<img src="<?php echo $icone['url']; ?>" alt="<?php echo rwmb_meta('Stardente_localizacao_endereco'); ?>">
						<figcaption class="hidden"><?php echo rwmb_meta('Stardente_localizacao_endereco'); ?></figcaption>
					</figure>
					<?php endforeach; $contador3++; endwhile; wp_reset_query(); ?>
				</div>
			</div>
		</div>
	</div>
</main>

<?php get_footer();

