<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Stardente
 */

$categoriaPost = get_the_category();

foreach($categoriaPost as $categoria) {
	$nomeCategoriaPost = $categoria->name;
	$idCategoriaPost = $categoria->cat_ID;
}

get_header();

?>

<div class="pg pg-post">
	<figure class="destaque" style="background-image: url(<?php echo wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' )[0] ?>);">
		<img src="<?php echo wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' )[0] ?>" alt="<?php echo wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' )[0] ?>" class="hidden">
		<figcaption class="hidden"><?php echo wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' )[0] ?></figcaption>
	</figure>
	<a href="<?= home_url('/blog/'); ?>" class="voltar"><img src="<?= get_template_directory_uri(); ?>/img/arrow-left (14)@1,5x.svg" alt=""></a>
	<div class="container">
		<article>
			<!-- <a href="<?php //echo get_category_link( $idCategoriaPost ); ?>" class="link-categoria"><?php //echo $nomeCategoriaPost; ?></a> -->
			<h2 class="titulo-post titulo"><?php echo get_the_title(); ?></h2>
			<span class="data"><?php the_time('j M Y'); ?></span>
			<?php while(have_posts()){ the_post(); the_content(); } ?>
		</article>
		<?php if ( comments_open() || get_comments_number() ) : comments_template(); endif; ?>
	</div>
</div>

<?php get_footer();