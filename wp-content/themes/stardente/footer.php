<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Stardente
 */

$localizacoes = new WP_Query(array( 'post_type' => 'localizacao', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1 ));
// $tratamentos_clinicos = new WP_Query(array( 'post_type' => 'tratamento', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1 ));
// $tratamentos_esteticos = new WP_Query(array( 'post_type' => 'tratamento-estetico', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1 ));

$tratamentos_esteticos = new WP_Query(
	array(
		'post_type' => 'tratamento',
		'posts_per_page' => -1, 
		'orderby' => 'id',
		'tax_query' => array(
			array(
				'taxonomy' => "categoriaTratamento",
				'field'    => 'slug',
				'terms'    => "estetica",
			)
		)
	)
);
							
$tratamentos_clinicos = new WP_Query(
	array(
		'post_type' => 'tratamento',
		'posts_per_page' => -1, 
		'orderby' => 'id',
		'tax_query' => array(
			array(
				'taxonomy' => "categoriaTratamento",
				'field'    => 'slug',
				'terms'    => "odontologia",
			)
		)
	)
);

global $configuracao;

?>

	<footer>
		
		<div class="large-container">
			<a href="#" class="logo-footer">
				<img src="<?php echo $configuracao['opt_logo_footer']['url']; ?>" alt="Logo">
			</a>
			<div class="row">
				<div class="col-md-2 sm-hidden">
					<div class="menu-footer">
						<h3 class="titulo">Stardente</h3>
						<nav>
							<?php
							global $wp;
							$current_url = home_url( add_query_arg( array(), $wp->request ) ); 

							if ($current_url === home_url()) {
								$menu = array(
									'theme_location'  => '',
									'menu'            => 'Menu footer',
									'container'       => false,
									'container_class' => '',
									'container_id'    => '',
									'menu_class'      => 'menu',
									'menu_id'         => '',
									'echo'            => true,
									'fallback_cb'     => 'wp_page_menu',
									'before'          => '',
									'after'           => '',
									'link_before'     => '',
									'link_after'      => '',
									'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
									'depth'           => 2,
									'walker'          => ''
								);
							} else{
								$menu = array(
									'theme_location'  => '',
									'menu'            => 'Menu footer paginas',
									'container'       => false,
									'container_class' => '',
									'container_id'    => '',
									'menu_class'      => 'menu',
									'menu_id'         => '',
									'echo'            => true,
									'fallback_cb'     => 'wp_page_menu',
									'before'          => '',
									'after'           => '',
									'link_before'     => '',
									'link_after'      => '',
									'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
									'depth'           => 2,
									'walker'          => ''
								);
							}
							wp_nav_menu( $menu );
							?>
							<!-- <a href="#secao-sobre" class="scrollTop">A Clínica</a>
							<a href="<?php// echo home_url('/blog/'); ?>" class="scrollTop">Blog</a>
							<a href="#secao-localizacao" class="scrollTop">Contato</a>
							<a href="#" class="scrollTop">Termos de Uso</a>
							<a href="#" class="scrollTop">Politica de Privacidade</a> -->
						</nav>
					</div>
				</div>
				<div class="col-md-2 sm-hidden">
					<div class="menu-footer t-clinico">
						<h3 class="titulo">Tratamentos Clínicos</h3>
						<nav>

							<?php while($tratamentos_clinicos->have_posts()): $tratamentos_clinicos->the_post(); ?>
							<a href="#secao-tratamentos-clinicos" class="scrollTop"><?php echo get_the_title(); ?></a>
							<?php endwhile; ?>

						</nav>
					</div>
				</div>
				<div class="col-md-3 sm-hidden">
					<div class="menu-footer t-estetico">
						<h3 class="titulo">Tratamentos Estéticos</h3>
						<nav>
							
							<?php while($tratamentos_esteticos->have_posts()): $tratamentos_esteticos->the_post(); ?>
							<a href="<?php echo get_permalink();?>" class="scrollTop"><?php echo get_the_title(); ?></a>
							<?php endwhile; ?>

						</nav>
					</div>
				</div>
				<div class="col-md-4">
					<div class="menu-footer local">
						<h3 class="titulo">Clínicas Curitiba e Araucária</h3>
						<ul>
							
							<?php while($localizacoes->have_posts()): $localizacoes->the_post(); ?>
							<li>
								<a href="<?php echo rwmb_meta('Stardente_localizacao_link_endereco'); ?>" target="_blank">
									<p><?php echo get_the_title(); ?></p>
									<p><?php echo rwmb_meta('Stardente_localizacao_endereco'); ?></p>
								</a>
								<span><?php echo rwmb_meta('Stardente_localizacao_telefone'); ?> / <?php echo rwmb_meta('Stardente_localizacao_celular'); ?></span>
							</li>
							<?php endwhile; ?>

						</ul>
					</div>
				</div>
				<div class="col-md-1">
					<div class="menu-footer r-social">
						<h3 class="titulo">Social Media</h3>
						<nav class="social-media">
							<a href="<?php echo $configuracao['opt_facebook'] ?>" target="_blank"><img src="<?php echo $configuracao['opt_facebook_icon']['url'] ?>" alt="Rede social" ></a>
							<a href="<?php echo $configuracao['opt_instagram'] ?>" target="_blank"><img src="<?php echo $configuracao['opt_instagram_icon']['url'] ?>" alt="Rede social"></a>
							<?php if( $configuracao['opt_twitter']):?>
							<a href="<?php echo $configuracao['opt_twitter'] ?>" target="_blank"><img src="<?php echo $configuracao['opt_twitter_icon']['url'] ?>" alt="Rede social"></a>
							<?php endif;?>
						</nav>
					</div>
				</div>
			</div>
			<div class="copyright">
				<div class="row">
					<div class="col-md-6">
						<p><?php echo $configuracao['opt_copyright']; ?></p>
					</div>
					<div class="col-md-6 text-right">
						<p><?php echo $configuracao['opt_desenvolvido_por']; ?> <img src="<?php echo $configuracao['opt_logo_gran']['url']; ?>" alt="Logo Gran"></p>
					</div>
				</div>
			</div>
		</div>
	</footer>

	<?php wp_footer(); ?>

</body>
</html>