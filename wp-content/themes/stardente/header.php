<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Stardente
 */

global $configuracao;

?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<!-- META -->
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<!-- FAVICON -->
	<link rel="shortcut icon" type="image/x-icon" href="<?php echo get_template_directory_uri() . '/img/favicon.ico'; ?>"/> 

	

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5DT8BL');</script>
<!-- End Google Tag Manager -->

</head>
<body <?php body_class(); ?>>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5DT8BL"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
		<?php wp_head(); ?>
	<header>
		<div class="large-container">
			<div class="row">
				<!-- LOGO -->
				<div class="col-md-2">
					<a href="<?php echo home_url(); ?>">
						<img class="img-responsive" src="<?php echo $configuracao['opt_logo_header']['url']; ?>" alt="Logo">
					</a>
				</div>
				<!-- MENU -->	
				<div class="col-md-8">
					<nav class="navbar navbar-expand-lg">
						<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
							<span class="navbar-toggler-icon"><img src="<?php echo get_template_directory_uri() . '/img/menu.svg'; ?>" alt="Ícone menu"></span>
						</button>

						<div class="collapse navbar-collapse" id="navbarSupportedContent">
							<?php
							global $wp;
							$current_url = home_url( add_query_arg( array(), $wp->request ) ); 

							if ($current_url === home_url()) {
								$menu = array(
									'theme_location'  => '',
									'menu'            => 'Menu principal',
									'container'       => false,
									'container_class' => '',
									'container_id'    => '',
									'menu_class'      => 'menu',
									'menu_id'         => '',
									'echo'            => true,
									'fallback_cb'     => 'wp_page_menu',
									'before'          => '',
									'after'           => '',
									'link_before'     => '',
									'link_after'      => '',
									'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
									'depth'           => 2,
									'walker'          => ''
								);
							} else{
								$menu = array(
									'theme_location'  => '',
									'menu'            => 'Menu principal paginas',
									'container'       => false,
									'container_class' => '',
									'container_id'    => '',
									'menu_class'      => 'menu',
									'menu_id'         => '',
									'echo'            => true,
									'fallback_cb'     => 'wp_page_menu',
									'before'          => '',
									'after'           => '',
									'link_before'     => '',
									'link_after'      => '',
									'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
									'depth'           => 2,
									'walker'          => ''
								);
							}
							wp_nav_menu( $menu );
							?>
						</div>
					</nav>
				</div>
				<!-- REDES SOCIAIS -->
				<div class="col-md-2">
					<ul class="redes-sociais">
						<li><a href="<?php echo $configuracao['opt_facebook'] ?>"><img src="<?php echo $configuracao['opt_facebook_icon']['url'] ?>" alt="Rede social"></a></li>
						<li><a href="<?php echo $configuracao['opt_instagram'] ?>"><img src="<?php echo $configuracao['opt_instagram_icon']['url'] ?>" alt="Rede social"></a></li>
						<?php if( $configuracao['opt_twitter']):?>
						<li><a href="<?php echo $configuracao['opt_twitter'] ?>"><img src="<?php echo $configuracao['opt_twitter_icon']['url'] ?>" alt="Rede social"></a></li>
					<?php endif;?>
					</ul>
				</div>
			</div>
		</div>
	</header>