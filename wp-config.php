<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'projetos_stardente_site' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '+cN#-|0c}c8wyhDez5P0$(-Ih<m6&-H| :v)?WQ4mA9,4WmC(G*(u a(~,J#0u5@' );
define( 'SECURE_AUTH_KEY',  'W:JGiOk6n|o2L4Lp@5Y!WY5Zeb*zc>dVeV,w0r^Gco~;Dn;EOjzRus=K0Y~ Fb~z' );
define( 'LOGGED_IN_KEY',    ' eujnhb:*Z,@ei5L:v/hG[gjZ@cNUP3/;}Bc@OLrj6^6pr;*53#AFp[SS:YOro8^' );
define( 'NONCE_KEY',        '2jeK26GYy;B<g2:*zq_Hf-./9!jF5R1PoNHT35ze*>2%1U?y{I*gSZ[;o<VX7a0e' );
define( 'AUTH_SALT',        'UZUB~LkA0g6iLo;ls:`bvmKZG@N:$wUr/aPdf+4ftyDbM][,a0)+?6f=N7)zjAW@' );
define( 'SECURE_AUTH_SALT', 'iCej)#]M2%7PV[3HN.auj.y>E`OMrs3a/Q2 s/;ywgzac/i>qp_GJQN0GuFqLv&P' );
define( 'LOGGED_IN_SALT',   ')+iPVP|x#Dm7Y;lwI<AP7WiiP]rs/s@Di(nuH +]um/=5Hhcd;t,HIG9*GKk|ptT' );
define( 'NONCE_SALT',       'u3 NAJoM#gK<f9g!Mq0?g `cZ<k`%a{*<=)o`z.-]3gu;W{8>`z_~wiR~d,FTVE>' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'sd_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
